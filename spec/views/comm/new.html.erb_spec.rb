require 'spec_helper'

describe "comm/new.html.erb" do
  it "displays fields to enter comm title and body" do
    assign(:post_id, 0)
    assign(:post_comm_index_path, "/posts/1/comm")
    render
    rendered.should have_selector("form", :method => "post", :action => post_comm_index_path)
    rendered.should have_selector("input", :value => "Nickname")
    rendered.should have_selector("input", :value => "Comment")
   
  end


end
