require 'spec_helper'

describe "posts/show.html.erb" do

  before do
#    @post = Post.create :title => "lala", :body => "lalala"
    assign(:post, stub_model(Post, :title => "sujet 1", :body => "lala"))
    assign(:comm, [stub_model(Comm, :nick => "name", :body => "lol")])

  end
  
  it "displays post title and body" do
    render
    rendered.should have_content "sujet 1"
    rendered.should have_content "lala"   
  end

  it "displays link to post a comment" do
    render
    rendered.should have_link "Add comment"
  end

end
