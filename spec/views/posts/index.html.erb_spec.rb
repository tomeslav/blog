require 'spec_helper'

describe "posts/index.html.erb" do

  before do
    assign(:posts, [
                    stub_model(Post, :title => "sujet 1", :body => "une histoire"),
                    stub_model(Post, :title => "sujet 2", :body => "une autre")
                   ])
  end
  
  it "displays all the posts" do
    render
    rendered.should =~ /sujet 1/
    rendered.should =~ /sujet 2/
  end

  it "give a link to create a new post if connected" do
    @session = 1
    render
    rendered.should =~ /New<\/a>/
  end
end
