require 'spec_helper'

describe "posts/new.html.erb" do
  it "displays fields to enter post title and body" do
    render
    rendered.should have_selector("form", :method => "post", :action => "/post")
    rendered.should have_selector("input", :value => "Title")
    rendered.should have_selector("input", :value => "Body")
   
  end

end
