require 'spec_helper'

describe CommController do
  it "should routes to #create on POST" do
    post('posts/42/comm').should route_to(:controller => 'comm', :action => 'create', :post_id => '42')
  end

  it "should route to #new" do
    get('posts/36/comm/new').should route_to(:controller => 'comm', :action => 'new', :post_id => '36')
  end
end

