require 'spec_helper'
describe PostsController do
  it "routes to #index" do
    get('/posts').should route_to("posts#index")
  end
  
  it "should provide the aliast post_path for /posts" do
    posts_path.should == '/posts'
  end
  
  it "routes to #new" do
    get('posts/new').should route_to("posts#new")
  end

  it "allow to post a new post and route to #create" do
    post('posts').should route_to("posts#create")
  end

  it "should route to #show" do
    get('posts/10').should route_to(:controller => 'posts', :action => 'show', :id => '10')
  end

end

