require 'spec_helper'

describe CommController do
  describe "GET 'new'" do
    it "render a comment form" do
      get 'new'
      response.should render_template(:new)
    end
  end

  describe "Post 'create'" do
    it "add a new comment" do
      post 'create', {:nickname => "toto", :body => "lala"}
      response.should be_redirect
    end
  end
end
