require 'spec_helper'

describe PostsController do
  describe "GET 'index'" do
    
    before(:each) do
      @posts = [stub_model(Post,:title => "1"), stub_model(Post, :title => "2")]
      Post.stub(:all){ @posts }
    end
    
    it "assigns a list of posts" do
      Post.should_receive(:all).and_return(@posts)
      get 'index'
      assigns(:posts).should eq @posts
      response.should be_success
    end
    
    it "renders the template list" do
      get 'index'
      response.should render_template(:index)
    end
  end
  
  describe "GET 'new'" do
    it "render a post posting entry" do
      session[:conn] = 1
      get 'new'
      response.should render_template(:new)
    end
  end

  describe "POST 'create'" do
    it "add the new post" do
      post 'create', {:title => "lala", :body => "foo"}
      response.should redirect_to(:posts)
    end
  end

  describe "GET 'show'" do
    it "shows the post" do
      post = Post.create(:title => "lala", :body => "toto")
      get 'show', :id => post.id
      response.should render_template(:show)
    end
  end

  describe "DELETE 'posts/:id'" do
    it "deletes the post" do
      post = Post.create(:title => "lala", :body => "un texte")
      delete :destroy, :id => post.id
#      response.should render_template(:index)
    end
  end
end
