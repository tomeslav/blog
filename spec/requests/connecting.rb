require 'spec_helper'

feature "Connecting" do

  describe "unconnected" do
    it "should not allow to remove a post" do
      visit posts_path
      page.should_not have_link "Delete"
    end
    it "should not allow to create a post" do
      visit posts_path
      page.should_not have_link "New"
    end
    it "should provide a link to log in" do
      visit posts_path
      page.should have_link "Connect"
    end
  end

  describe "connected" do

    before do
      page.set_rack_session(:conn => 1)
      visit posts_path
    end
    
    it "should allow to create a new post" do
      page.should have_link "New"
    end
    
    it "should allow to delete a post" do
      page.should have_link "Delete"
    end

    it "should allow do disconnect" do
      page.should have_link "Disconnect"
    end
  end
end
