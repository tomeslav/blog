require 'spec_helper'

describe "RemovingPosts" do
    before(:each) do
      @post1 = Post.create(:title => "sujet1", :body => "bla bla")
      @post2 = Post.create(:title => "sujet2", :body => "bla bla")
    end

  describe "GET /posts" do
    it "should allow to remove a post" do
      page.set_rack_session(:conn => 1)
      visit posts_path
      page.should have_content @post1.title
      page.should have_content @post2.title
      click_link 'Delete'
      page.should have_content @post2.title
    end
  end
end
