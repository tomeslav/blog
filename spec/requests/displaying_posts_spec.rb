require 'spec_helper'

describe "DisplayingPosts" do
  before do
      @post = Post.create(:title => "sujet1", :body => "bla bla")
  end
  
  describe "GET /posts/:id"  do
    it "should display the post" do
      visit post_path(@post.id)
      page.should have_content @post.title
      page.should have_content @post.body
    end

    it "should display a link to post a comment" do
      visit post_path(@post.id)
      page.should have_link "Add comment"
    end

    it "should display the comments" do
      @comm = Comm.create(:nick => "toto", :body => "lol", :post_id => @post.id)
      visit post_path(@post.id)
      page.should have_content @comm.nick
      page.should have_content @comm.body
    end
  end
end
