require 'spec_helper'

describe "Create Comment" do
  before(:each) do
    @post = Post.create(:title => "sujet1", :body => "bla bla")
  end
  
  describe "Post /posts/:post_id/comm" do
    it "should allow to create a new comment and display it"   do
      visit post_path(@post.id)
      click_link "Add comment"
      fill_in 'n', :with => "toto"
      fill_in 'b', :with => "mdr"
      click_button 'Debate'
      current_path.should == post_path(@post.id)
      page.should have_content 'mdr'
    end
  end
  
end
