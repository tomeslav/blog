class CreateComms < ActiveRecord::Migration
  def change
    create_table :comms do |t|
      t.string :nick
      t.string :body
      t.integer :post_id
      t.timestamps
    end
  end
end
