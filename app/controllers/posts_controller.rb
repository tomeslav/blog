# -*- coding: utf-8 -*-
class PostsController < ApplicationController
  
  def index
    @posts = Post.all
    if params[:auth]
      session[:conn] = 1
      params[:auth] = nil
    end
    if params[:disc]
      session[:conn] = nil
      params[:disc] = nil
    end
    @session = session[:conn]
  end
  
  def new
    if session[:conn]
      @posts = Post.all
      respond_to do |format|
        format.html # new.html.erb
        format.js
      end
    else
      redirect_to posts_url
    end
  end
  
  def create
    if session[:conn]
      Post.create(:title => params[:t], :body => params[:b])
      flash[:notice] = "Post " + params[:t] + " crée"
      redirect_to :posts
    else
      redirect_to posts_url
    end
  end
  
  def show
    @post = Post.find params[:id]
    @comm = Comm.find_all_by_post_id(@post)
    respond_to do |format|
      format.js
      format.html
    end
  end

  def destroy
    if session[:conn]
      flash[:notice] = "Post " + params[:id] + " supprimé"
      Post.find_by_id(params[:id]).delete
      Comm.find_all_by_post_id(params[:id]).each {|c| c.delete}
      @posts = Post.all
      @session = session[:conn]
      respond_to do |format|
        format.js
        format.html # pour le test…
      end
    else
      redirect_to posts_url
    end  
  end
end
