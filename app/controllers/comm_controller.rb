# -*- coding: utf-8 -*-
class CommController < ApplicationController
  def new
    respond_to do |format|
      format.js
      format.html # pour faire passer le test…
    end
  end
  
  def create
    Comm.create(:nick => params[:n], :body => params[:b], :post_id => params[:post_id])
    redirect_to ["/posts/",params[:post_id]].join
  end
end
